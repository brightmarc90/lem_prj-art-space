//Sliders
//position actuelle de chaque slider
let sartInd = 1;
let blogInd = 1; 
//recupération des slides de chaque slider
const slidesStart = Array.from(document.querySelectorAll(".slider-start li"));
const slidesBlog = Array.from(document.querySelectorAll(".slider-blog li"));

//activation du slide selectionné et desactivation des autres
function showSlides(ind, slidesTab) {
    let i;
    let slides = slidesTab;
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[ind - 1].style.display = "block";    
}

//récupération des boutons de gauche et application d'un écouteur d'évenements au clic
const btnLeftTab = Array.from(document.querySelectorAll(".btn-left"));
btnLeftTab.forEach((element, ind) => {
    element.addEventListener("click", ()=>{
        if (ind == 0) {   // cas slider start 
            if (sartInd - 1 > slidesStart.length) { sartInd = 1 }
            if (sartInd - 1 < 1) { sartInd = slidesStart.length }        
            showSlides(sartInd--, slidesStart)
        } else { // cas slider blog 
            if (blogInd - 1 > slidesBlog.length) { blogInd = 1 }
            if (blogInd - 1 < 1) { blogInd = slidesBlog.length }
            showSlides(blogInd--, slidesBlog)
        }
    });
});

//récupération des boutons de droite et application d'un écouteur d'évenements au clic
const btnRightTab = Array.from(document.querySelectorAll(".btn-right"));
btnRightTab.forEach((element, ind) => {
    element.addEventListener("click", () => {
        if (ind == 0) { // cas slider start 
            if (sartInd + 1 > slidesStart.length) { sartInd = 1 }
            if (sartInd + 1 < 1) { sartInd = slidesStart.length }
            showSlides(sartInd++, slidesStart)
        } else { // cas slider blog 
            if (blogInd + 1 > slidesBlog.length) { blogInd = 1 }
            if (blogInd + 1 < 1) { blogInd = slidesBlog.length }
            showSlides(blogInd++, slidesBlog)
        }
    });
});

// lancement du slider
function sliderStart() {
    showSlides(sartInd, slidesStart)
    showSlides(blogInd, slidesBlog)
}

sliderStart();

