// récupération des tooltip gauche et droite
let toolTipLeft = document.querySelector("#section1 .left.tool-tip");
let toolTipRight = document.querySelector("#section1 .right.tool-tip");
// console.log(toolTipLeft, toolTipRight)

// récupération des 2 blocs d'offres
const offersBlocks = Array.from(document.querySelectorAll("#section1 .price-card"));
// console.log(offersBlocks)

//
function moveToolTip(e, ind) {
    if (ind == 0) {
        toolTipLeft.style.display = "block";
        toolTipLeft.style.left = e.pageX + "px";
        toolTipLeft.style.top = e.pageY + "px";
    } else {
        toolTipRight.style.display = "block";
        toolTipRight.style.left = e.pageX + "px";
        toolTipRight.style.top = e.pageY + "px";
    }
}
function hideToolTip(e, ind) {
    if (ind == 0) {
        toolTipLeft.style.display = "none";
    } else {
        toolTipRight.style.display = "none";
    }
}

offersBlocks.forEach((element, ind) => {
    element.addEventListener("mousemove", (e)=>{
        if (window.innerWidth >= 992 ){
            moveToolTip(e, ind)
        }        
        // console.log(e.pageX, e.pageY);      
    })
});

offersBlocks.forEach((element, ind) => {
    element.addEventListener("mouseleave", (e) => {
        hideToolTip(e, ind)
        // console.log(e.pageX, e.pageY);
    })
});

//récupération des boutons plus d'infos
const btnMoreInfos = Array.from(document.querySelectorAll(".more-info"));
let lttOpen = false, rttOpen = false;
//récupération des tooltip à afficher en petits écrans
const littleToolTips = Array.from(document.querySelectorAll("div[class*=-tool-tip"));

btnMoreInfos.forEach((element,ind) => {
    element.addEventListener("click", ()=>{
        if(ind == 0){
            if(lttOpen){
                littleToolTips[0].style.display = "none";
                lttOpen = false
            }else{
                littleToolTips[0].style.display = "block";
                lttOpen = true
            }            
        }
        else{
            if(rttOpen){
                littleToolTips[ind].style.display = "none";
                rttOpen = false
            }else{
                littleToolTips[ind].style.display = "block";
                rttOpen = true
            }
            
        }
    })
});